# To run

```
git clone git@gitlab.com:Stenters/sdl-tech-assign.git
cd sdl-tech-assign
npm install
npm start
```

Then navigate to localhost:3000 to see the magnificence that is raw HTML

To test it's working as expected, make sure that all on screen prompts do what you'd expect them to, and your terminal isn't a mess of red

# Goal

This technology assignment started off as an attempt to see if it was possible for a judge to have the server download students' code, run it with a fixed input, compare to an expected output, then print the results. Partway through, I was told this feature was very far down the road if it was going to happen at all. So I pivoted and started working on an ORM integration, complete with wrapper functions to streamline DB I/O

# Tech tried

For the duration, I worked with a barebones Express project written in Typescript (TS was precomplied into JS so everything ran smoothly). For the first part I was working with raw HTML for file uploading, multer for processing file uploads, and LokiJS for saving data server side (from following a tutorial). I then used a node package ('child_process') to create a shell to execute the script (security? what's a security?). 

When I pivoted to ORM stuff, I utilized the Sequelize node module as my ORM hooked up to a SQLite DB. This late in I didn't introduce any new technologies, instead making sure that I could get what I had running smoothly.

# Lessons learned

I learned a lot from this. This was my first experience with TypeScript, and while I ran into some problems with including packaged, it worked out smoothly overall. I also hadn't used Angular, and from what I saw from tutorials, it seems very intuitive. I can now create a server that can handle uploading files (although I'd not do what I did here if I had a choice). And best of all, I now am familiar with SQLite ORM integration into TS projects.
