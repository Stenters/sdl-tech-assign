import e = require("express");
import { ne } from "sequelize/types/lib/operators";
import { Json } from "sequelize/types/lib/utils";

const { Sequelize, Model, DataTypes } = require('sequelize');
const sequelize = new Sequelize('sqlite::memory', {
    define: {
        freezeTableName: true,
        timestamps: true
    }
});

export class Thread extends Model {}
Thread.init({
    originalPoster: DataTypes.STRING,
    title: DataTypes.STRING,
    content: DataTypes.JSON
}, { sequelize });


export async function init(): Promise<void> {
    await sequelize.sync();
    console.log("Tables created!");
}

export async function createThread(user:string, title:string, comment:string): Promise<number> {
    const newThread = await Thread.create({
        originalPoster: user,
        title: title,
        content: {comment:comment,replies:[]}
    })

    return newThread.id;
}

export async function addToThread(tId:number, comment:string, uname:string): Promise<void> {
    const threadToUpdate = await Thread.findByPk(tId);
    threadToUpdate.dataValues.content.replies.push({user:uname,comment:comment});
    threadToUpdate.changed('content', true);
    await threadToUpdate.save();
    console.log('saved replies to db')
    let t = await Thread.findByPk(tId)
    console.log(`new replies is ${t.dataValues.content.replies}`)
}

export async function getThread(tId:number) {
    let t = await Thread.findByPk(tId);
    return t.dataValues;
    // return Thread.findAll({where: {id: tId}})[0].dataValues;
}