import * as del from 'del';
import * as Loki from 'lokijs';
import * as child from 'child_process';
import * as fs from 'fs';
import * as path from 'path';

const loadCollection = function (colName, db: Loki): Promise<Loki.Collection<any>> {
    return new Promise(resolve => {
        db.loadDatabase({}, () => {
            const _collection = db.getCollection(colName) || db.addCollection(colName);
            resolve(_collection);
        })
    });
}

const programOnly = function (req, file, cb) {
    // accept prog only
    if (!file.originalname.match(/\.(py|cs|js|java|c|cpp)$/)) {
        return cb(new Error('Only the following files are allowed: .py,.cs,.js,.java,.c,.cpp'), false);
    }
    cb(null, true);
};

const cleanFolder = function (folderPath) {
    // delete files inside folder but not the folder itself
    del.sync([`${folderPath}/**`, `!${folderPath}`]);
}

const runFile = function(prog, input, output, res) {
    let dir = path.join(__dirname + '/../uploads/');

    // Read the expected output
    fs.readFile(dir + output['filename'], (err, data) => {
        if (err) {
            console.error(err);
            return `unexpected error in reading output: ${err}`;
        }
        
        // Now that you have that, execute the program with the given input
        child.exec(`python ${dir + prog['filename']} < ${dir + input['filename']}`, (err, stdout, stderr) => {
            if (err) {
                console.error(err);
                return `unexpeced error in exec: ${err}`;
            }
            // console.log(`\treturning value:\nstdout: ${stdout.trim()}\nexpected: '${data}'\nequal?: ${stdout.trim() === data.toString()}\n\nstderr: ${stderr}`);

            // Send back the comparison
            res.send(`stdout: ${stdout}<br>expected: ${data}<br>equal?: ${stdout.trim() === data.toString()}<br><br>stderr: ${stderr}`);
        });
    });
    
    

  
}

export {loadCollection, cleanFolder, runFile, programOnly}