import * as express from 'express';
import * as multer from 'multer';
import * as cors from 'cors';
import * as fs from 'fs';
import * as path from 'path';
import * as Loki from 'lokijs';
import * as bodyParser from 'body-parser';
import * as utils from './utils';
import * as database from './db';

// setup
const DB_NAME = 'db.json';
const COLLECTION_NAME = 'files';
const UPLOAD_PATH = 'uploads';
const upload = multer({ dest: `${UPLOAD_PATH}/` }); // multer configuration
const db = new Loki(`${UPLOAD_PATH}/${DB_NAME}`, { persistenceMethod: 'fs' });

// app
const app = express();
app.use(cors());
app.use(express.static('./public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// routes

//-- /

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + '/../public/index.html'));
})

//-- exec

app.get('/exec', (req,res) => {
    res.sendFile(path.join(__dirname + '/../public/fileForm.html'));
})

app.post('/exec', upload.fields([ {name: 'inputFile', maxCount: 1}, 
                                  {name: 'progFile', maxCount: 1},
                                  {name: 'outputFile', maxCount: 1} ]), async (req, res) => {

    let input  = req.files['inputFile' ][0];
    let prog   = req.files['progFile'  ][0];
    let output = req.files['outputFile'][0];

    let files = {input, prog, output};

    try {
        const col = await utils.loadCollection(COLLECTION_NAME, db)
        let data = [].concat(col.insert(files));

        db.saveDatabase();
        res.redirect('/run');
    } catch (err) {
        res.sendStatus(400);
    }
})

//-- run

app.get('/run', async (req,res) => {
    try {
        const col = await utils.loadCollection(COLLECTION_NAME, db);

        
        let log: string = '';
        let html: string = '<!DOCTYPE html><head><title>Run</title></head><body><table><tr><th>id</th><th>prog</th><th>input</th><th>output</th><th>link</th></tr>';
        
        col.chain().data().forEach((val) => {
            log += `\n\tid: ${val['$loki']}` + 
                   `\n\tprog: ${val['prog']['originalname']}` +
                   `\n\tin: ${val['input']['originalname']}` +
                   `\n\tout: ${val['output']['originalname']}\n`;

            html += `<tr><th>${val['$loki']}</th>` +
                    `<th>${val['prog']['originalname']}</th>` +
                    `<th>${val['input']['originalname']}</th>` +
                    `<th>${val['output']['originalname']}</th>` +
                    `<th><a href="/run/${val['$loki']}">run</a></th></tr>`
        });

        html += '</table></body>';

        console.log(log);

        fs.writeFile(path.join(__dirname, '/../public/run-table.html'), html, (err) => {
            if (err) {
                console.error(err);
                res.sendStatus(400);
                return;
            }

            res.sendFile(path.join(__dirname, '/../public/run-table.html'));
        });
        

    } catch (err) {
        res.sendStatus(400);
    }
})

app.get('/run/:id', async (req,res) => {
    try {
        const col = await utils.loadCollection(COLLECTION_NAME, db);
        const result = col.get(+req.params.id);

        if (!result) {
            res.sendStatus(404);
            return;
        };

        utils.runFile(result['prog'], result['input'], result['output'], res);

    } catch (err) {
        res.sendStatus(400);
    }
})

//-- comments

app.get('/comments', async (req, res) => {
    console.log("sending comments.html");
    res.sendFile(path.join(__dirname + '/../public/comments.html'));
})

app.post('/comments', async (req, res) => {
    let text: string = req.body.inputText,
        title: string = req.body.inputTitle,
        name: string = req.body.inputName;

    let tId: number = await database.createThread(name,title,text);
    
    // append to comments.html
    let commentsHTML: string = fs.readFileSync(path.join(__dirname + '/../public/comments.html')).toString();
    let toAppend: string = `<a href="comments/${tId}">${name}: ${title}</a> <br>`;

    // write, then redirect to comment
    fs.writeFile(path.join(__dirname + '/../public/comments.html'), commentsHTML + toAppend, (err) => {
        if (err) {
            console.error(err);
            res.sendStatus(400);
            return;
        }

        res.redirect('/comments/' + tId);
    });    
})

app.get('/comments/:threadid', async (req, res) => {
    let tId: number = +req.params.threadid;
    let thread = await database.getThread(tId);
    let originalPoster: string = thread.originalPoster,
        title: string = thread.title,
        content: any = thread.content,
        createdAt: Date = thread.createdAt;
        
    let html: string = `<h1>${title}</h1><p>(${createdAt})</p><br>` +
        `<p><b>${originalPoster}:</b> ${content.comment}</p> <br>`;
    
    console.log(`content is\n${JSON.stringify(content)}\nreplies are\n${content.replies}`)
    
    for (let i: number = 0; i < content.replies.length; ++i ) {
        console.log(">>inserting: " + content.replies[i].user + ": " + content.replies[i].comment)
        html += `<p><b>${content.replies[i].user}:</b> ${content.replies[i].comment}</p> <br>`;
    }

    html += `<form action="/comments/${tId}" method="POST">` +
    `<label for="inputName">Name:</label>` +
    `<input name="inputName" type="text">` +
    `<label for="inputComment">Comment:</label>` +
    `<input name="inputComment" type="text">` +
    `<input type="submit"></form> <br>` +
    `<a href="/comments">Go Back</a> <br>`;
    
    fs.writeFile(path.join(__dirname + `/../public/comment${tId}.html`), html, (err) => {
        if (err) {
            console.error(err);
            res.sendStatus(400);
            return;
        }

        res.sendFile(path.join(__dirname + `/../public/comment${tId}.html`));
    });
})

app.post('/comments/:threadid', async (req, res) => {
    let tId: number = +req.params.threadid;
    let comment: string = req.body.inputComment,
        name: string = req.body.inputName;

    await database.addToThread(tId,comment,name);
    res.redirect('/comments/' + tId);
})

// start the server
app.on('ready', () => {
    console.log('listening on port 3000!');
    utils.cleanFolder(UPLOAD_PATH);
    app.listen(3000);
})


// make sure db is inited before running the app
function start(): void {
    database.init().then(() => {
        app.emit('ready');
    })
}

start();
