const http = require("http");
const express = require('express');
const path = require('path');
const busboy = require('connect-busboy');
const fs = require('fs-extra');
const multer = require('multer');

const app = express();
const port = 8000;

app.use('/files', express.static('public'))
app.use(busboy())
app.use(multer({dest: './uploads'}).array('fileIn'));

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
  });

app.get('/', (req,res) => {
    res.redirect('/exec');
})

app.get('/exec', function(req,res) {
    console.log("getting /exec");
    res.sendFile(path.join(__dirname + '/public/form.html'));
})

app.post('/exec', (req, res) => {
    // var fstream;
    // req.pipe(req.busboy);
    // req.busboy.on('file', (fieldname, file, filename) => {
    //     console.log(`uploading ${filename}`);
    //     fstream = fs.createWriteStream(__dirname + '/uploads' + filename);
    //     file.pipe(fstream);
    //     fstream.on('close', () => {
    //         console.log(`finished uploading ${filename}`);
    //         res.redirect('back');
    //     })
    // })
    // Breaking in _stream_readable.js:681 (dest undefined)
    console.log('uploading file...');
    console.dir(req.files);
    res.send('upload complete');

   
})

app.listen(port, () => {
    console.log(`Express running on port ${port}`)
})